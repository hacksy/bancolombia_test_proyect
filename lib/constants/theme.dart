import 'package:flutter/material.dart';
import 'package:test_project/constants/colors.dart';

ThemeData themeData = ThemeData(
  primarySwatch: Colors.blue,
  accentColor: BancolombiaColors.brilliantRed,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);
